OSI Binary Trace File Player Flat
===================================

This [FMU](https://fmi-standard.org/) is able to play binary OSI trace files that have been serialized with [Flatbuffers](https://google.github.io/flatbuffers/) and are located under _/tmp/osi_.
It is build according to the [ASAM Open simulation Interface (OSI)](https://github.com/OpenSimulationInterface/open-simulation-interface) and the [OSI Sensor Model Packaging (OSMP)](https://github.com/OpenSimulationInterface/osi-sensor-model-packaging) 
examples.

An exemplary trace file is available in folder _trace_file_examples_.

## Installation

### Dependencies

Install `cmake` 3.10.2:

```bash
$ sudo apt-get install cmake
```

### Clone with submodules

```bash
$ git clone https://gitlab.com/persival-open-source/open-simulation-interface/osi-trace-file-player-flat.git
$ cd osi-trace-file-player-flat
$ git submodule update --init
```

### Build

```bash
$ mkdir -p build
$ cd build
$ cmake ..
$ cmake --build .
```
